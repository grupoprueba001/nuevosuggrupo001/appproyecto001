package com.pe.springboot.apirest.models.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
@AllArgsConstructor
public @Data class Cliente implements Serializable{
/**
* 
*/
private static final long serialVersionUID = 6833709172962605277L;
private Long  id;
private  String nombre;
private  String apellido;
private  String email;
private  Date fechaCreacion;
}
